Overview
--------

cuff is a suite of blockchain tools for tracking and analysis.

Bitcoin and Etherium are currently supported.

cuff uses John-David Dalton's forward thinking and elegant
[@std/esm](https://www.npmjs.com/package/@std/esm) module for ES6 imports and
exports on the back end; no transpilation step is required.

Installation
------------

`$ npm i --save cuff`

or

`$ yarn add cuff`

You might need to add `.esm-cache` to your `.gitignore`

Methods and Usage
-----------------

Bring relevant classes into your namespace.

```javascript
const { Bitcoin, Etherium } = require('cuff');
```

 Below is a list of currently available methods. All methods return a promise.

```javascript
// top level data for the current state of the coin's blockchain
Bitcoin.blockchainInfo()
Etherium.blockchainInfo()

// data for a specific block in the chain
Bitcoin.blockInfo(blockHash)
Etherium.blockInfo(blockHash);

// data for specific transaction
Bitcoin.txInfo(txId)
Etherium.txInfo(txId);
```

Examples
--------

```javascript
const { Bitcoin, Etherium } = require('cuff');

Bitcoin.blockchainInfo()
  .then(data => {
    console.info(data);
  })
  .catch(err => {
    console.info('Error:', err)
  });


Etherium.blockchainInfo()
  .then(data => {
    console.info(data);
    })
    .catch(err => {
      console.info('Error:', err);
    });
```

Sample Output
-------------

```javascript
Bitcoin.blockchainInfo().then(data => console.info(data));

{
  "name": "BTC.main",
  "height": 488733,
  "hash": "0000000000000000005d1ec542fda806c5ee9a27781812e161f6ae135d1b825d",
  "time": "2017-10-07T17:53:09.761213067Z",
  "previous_hash": "000000000000000000e39bd308bca48bb5339fc52ba246323870839f33c7421d",
  "peer_count": 994,
  "unconfirmed_count": 12845,
  "high_fee_per_kb": 124705,
  "medium_fee_per_kb": 110000,
  "low_fee_per_kb": 90000,
  "last_fork_height": 487958,
  "last_fork_hash": "0000000000000000005c5028bbbc9f3d472e85144f2698ad9fb4e1dd95fc9d93"
}
```

```javascript
const txId = '45b263b0ebb9acefc78f9bfdcdbdbf3cd1439cb8344d52b84ed793d67726a224';
Etherium.txInfo(txId).then(data => console.info(data));

{
  "block_hash": "0f1803b7e29a4b73a3cd69913d67b037249284094202e68c6e82b6aee0bc9155",
  "block_height": 4347981,
  "block_index": 56,
  "hash": "45b263b0ebb9acefc78f9bfdcdbdbf3cd1439cb8344d52b84ed793d67726a224",
  "addresses": [
    "3b0bc51ab9de1e5b7b6e34e5b960285805c41736",
    "f4e659f164044b65f28739b34c67a8d3644ad32e"
  ],
  "total": 511638030000000000,
  "fees": 630000000000000,
  "size": 113,
  "gas_limit": 50000,
  "gas_used": 21000,
  "gas_price": 30000000000,
  "confirmed": "2017-10-08T15:15:57Z",
  "received": "2017-10-08T15:15:11.945Z",
  "ver": 0,
  "double_spend": false,
  "vin_sz": 1,
  "vout_sz": 1,
  "confirmations": 238,
  "confidence": 1,
  "inputs": [
    {
      "sequence": 73373,
      "addresses": [
        "3b0bc51ab9de1e5b7b6e34e5b960285805c41736"
      ]
    }
  ],
  "outputs": [
    {
      "value": 511638030000000000,
      "addresses": [
        "f4e659f164044b65f28739b34c67a8d3644ad32e"
      ]
    }
  ]
}
```

Custom Response Payloads
------------------------

What if you don't need every property in the response object? Maybe you want to
be lean and mean: you need only a few keys. With an optional list of object
properties included in the call, say good riddance to unwanted clutter!

```javascript
Bitcoin.blockchainInfo('time', 'peer_count').then(data => console.info(data));

{
  "time": "2017-10-07T17:53:09.761213067Z",
  "peer_count": 994
}

// Array syntax is also supported
const txId = '45b263b0ebb9acefc78f9bfdcdbdbf3cd1439cb8344d52b84ed793d67726a224';
const props = ['gas_limit', 'gas_used', 'gas_price'];
Etherium.txInfo(txId, props).then(data => console.info(data));

{
  "gas_limit":50000,
  "gas_used":21000,
  "gas_price":30000000000
}
```

The Future
----------

cuff's philosophy is to maintain currency with the avant-garde progress of the
trustworthy economy. We are likewise committed to providing simple and clear
interfaces for the developer.

More extensive functionality will be forthcoming.

Have fun!

Gerry Gold  
October 2017
