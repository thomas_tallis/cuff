import rp from 'request-promise';
import _pick from 'lodash.pick';

const BASE_URL = 'https://api.blockcypher.com/v1';
const METHOD_ERROR_CODE = 3405691582;

export default class BaseApi {
  constructor(coinCode) {
    this.coinCode = coinCode;
  }

  /**
   * Returns metadata about the current state of the coin's blockchain
   * @param  {Array} props return result set containing only props
   * @return {Promise} resolves to JSON string of an object
   */
  blockchainInfo(...props) {
    const url = `${BASE_URL}/${this.coinCode}/main`;

    if (props.length > 0) {
      return BaseApi.filterResultProperties(url, props);
    }

    return rp(url);
  }

  /**
   * Returns data for a single block in the chain
   * @param  {String} blockHash the hash of the block, SHA256(SHA256(block))
   * @param  {Array} props return result set containing only props
   * @return {Promise} resolves to JSON string
   */
  blockInfo(blockHash, ...props) {
    const url = `${BASE_URL}/${this.coinCode}/main/blocks/${blockHash}`;

    if (typeof blockHash !== 'string') {
      return BaseApi.methodErrorHandler('blockInfo method missing blockHash arg');
    } else if (props.length > 0) {
      return BaseApi.filterResultProperties(url, props);
    }

    return rp(url);
  }

  /**
   * Returns data for a single tx
   * @param  {String} txId hash
   * @param  {Array} props return result set containing only props
   * @return {Promise} resolves to JSON string
   */
  txInfo(txId, ...props) {
    const url = `${BASE_URL}/${this.coinCode}/main/txs/${txId}`;

    if (typeof txId !== 'string') {
      return BaseApi.methodErrorHandler('txInfo method missing txId arg');
    } else if (props.length > 0) {
      return BaseApi.filterResultProperties(url, props);
    }

    return rp(url);
  }

  /**
   * The properties will either be an array of strings:
   *     ['name', 'hash', 'time']
   *
   * or a nested array as a single element in the array:
   *     [ ['name', 'hash', 'time'] ]
   *
   * @param {String} url
   * @param  {Array} props
   * @return {Promise} promise
   */
  static filterResultProperties(url, props) {
    // Since we know the exact optional nestedness structure, this is the
    // simplest flattening operation.
    const properties = Array.isArray(props[0]) ? props[0] : props;
    return rp(url).then((result) => {
      const originalResult = JSON.parse(result);
      return JSON.stringify(_pick(originalResult, properties));
    });
  }

  /**
   * This is called if arg is invalid, avoiding an unnecessary HTTP request to
   * the main API.
   * @return {Promise} rejects to JSON string
   */
  static methodErrorHandler(message) {
    return new Promise((resolve, reject) => {
      reject(JSON.stringify({ errorCode: METHOD_ERROR_CODE, message }));
    });
  }
}
