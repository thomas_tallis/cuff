class Utils {
  static isPositiveInteger(n) {
    return Number.isInteger(n) && n > 0;
  }
}

module.exports = Utils;
