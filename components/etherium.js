import BaseApi from '../common/base-api';

const COIN_CODE = 'eth';

class Etherium extends BaseApi {
  constructor() {
    super(COIN_CODE);
  }
}

export default new Etherium();
