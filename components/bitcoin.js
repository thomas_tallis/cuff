import BaseApi from '../common/base-api';

const COIN_CODE = 'btc';

class Bitcoin extends BaseApi {
  constructor() {
    super(COIN_CODE);
  }
}

export default new Bitcoin();
