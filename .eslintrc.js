module.exports = {
  'extends': 'airbnb',

  // Override some of airbnb's style guides.
  'rules': {
    // allow 'js' extension for node module imports via '@std/esm' mechanism
    'import/extensions': 0,

    'func-names': ['error', 'never'],

    // console statements are part of cuff's command line context
    'no-console': 0,

    // allow "for of" loops
    'no-restricted-syntax': 0,

    // Disallow final comma in list; this is consistent with JSON requirements,
    // thus avoiding yet another syntax-based context switch when manually
    // editing JSON. (Yes, a diff will have an extra line when a hard-coded list
    // is appended: it's a tiny tradeoff).
    'comma-dangle': ['error', 'never']
  },
  'env': {
    'es6': true,
    'node': true
  }
};
